CREATE OR REPLACE TRIGGER TG_AUDITO_TOTALRESERVA AFTER UPDATE OF V_TOTAL ON RESERVA
FOR EACH ROW
DECLARE 
   l_pk  auditoria.k_audito%TYPE;
 
BEGIN

  select  alquiler.SEQ_ALQUILER.NEXTVAL into l_pk from dual;

  INSERT INTO AUDITORIA VALUES (l_pk, 
                                SYSDATE,
                                USER,
                                K_RESERVA,
                                K_NIT,
                                V_TOTAL,
                                V_TOTAL
  );


END TG_AUDITO_TOTALRESERVA;


/


show errors;

exit;

