DROP TABLE alquiler.auditoria;


CREATE TABLE alquiler.auditoria
(
	K_AUDITO       NUMBER(11)     NOT NULL,    -- Número de la secuencia
	F_ACTUALIZA    DATE           NOT NULL,        -- Fecha del sistema 
	N_USUARIO      VARCHAR2(10)   NOT NULL,    -- usuario conectado a la sesión
        K_RESERVA      NUMBER(10)     NOT NULL,   -- nÚM DE RESERVA
        K_NIT          NUMBER(12)     NOT NULL, -- Nit del cliente
        V_ANTOTAL      NUMBER(10,2)    NOT NULL, -- Total anterior de la reserva
        V_NEWTOTAL      NUMBER(10,2)    NOT NULL, -- Total nuevo de la reserva 
)
TABLESPACE	DEF_ALQUILER
;

