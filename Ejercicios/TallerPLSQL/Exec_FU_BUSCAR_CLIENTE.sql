/*
  Bloque anónimo para ejecutar función
*/

SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED;
SET VERIFY OFF;

DECLARE
    lk_nit            NUMBER:=&r;
    lc_error          NUMBER:=0;
    lm_error          VARCHAR2(200);
    lv_salida         VARCHAR2(200);

BEGIN
    
    lv_salida := PK_RESERVAS_TALLER.FU_BUSCAR_CLIENTE(lk_nit, lc_error, lm_error);
    DBMS_OUTPUT.put_line('ok');
    DBMS_OUTPUT.put_line(lv_salida);
END;

/

exit;

