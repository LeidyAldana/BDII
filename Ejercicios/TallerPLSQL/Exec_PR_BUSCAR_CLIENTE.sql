/*
  Bloque anónimo para ejecutar procedimiento
*/

SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED;
SET VERIFY OFF;

DECLARE
    lk_nit            NUMBER:=&r;
    lr_cliente        ALQUILER.PK_RESERVAS_TALLER.GTR_CLIENTE; 
    lc_error          NUMBER:=0;
    lm_error          VARCHAR2(200);


BEGIN
    
    PK_RESERVAS_TALLER.PR_BUSCAR_CLIENTE(lk_nit, lr_cliente, lc_error, lm_error);
    DBMS_OUTPUT.put_line('ok');

END;

/

exit;

