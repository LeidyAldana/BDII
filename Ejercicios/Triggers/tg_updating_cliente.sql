
CREATE OR REPLACE TRIGGER TG_CAMBIAR_NOMBRE
BEFORE UPDATE OF N_NOMCLIENTE, N_APECLIENTE ON CLIENTE
FOR EACH ROW 
-- DECLARE
BEGIN
-- Modificar para que solo cambie lo que se indica, para que no vuelva en mayuscula 

IF (:new.N_NOMCLIENTE <> :old.N_NOMCLIENTE) THEN
  :new.N_NOMCLIENTE := UPPER(:new.N_NOMCLIENTE);
  DBMS_OUTPUT.PUT_LINE('Ejecutando trigger1');
END IF;

-- aPLLIDO
IF (:new.N_APECLIENTE <> :old.N_APECLIENTE) THEN
  :new.N_APECLIENTE := UPPER(:new.N_APECLIENTE);
  DBMS_OUTPUT.PUT_LINE('Ejecutando trigger2');
END IF;


END TG_CAMBIAR_NOMBRE;


/


show errors;

exit;
