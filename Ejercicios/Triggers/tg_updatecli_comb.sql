
CREATE OR REPLACE TRIGGER TG_CAMBIAR_NOMBRE
-- Before , after, insetad of no pueden combinarse, se usa una sola
-- before: Completa variables
-- After : Guaradar resultado en otro lado
- Instead of: Solo con vistas
- Before y after: solo tablas
- Las vistas son actualizables y hay que darles seguridad (privilegios)
- Las vistas materializadas no son actualizables
-- update, insert , delete si pueden combinarse

BEFORE UPDATE OR INSERT OF N_NOMCLIENTE, N_APECLIENTE ON CLIENTE
FOR EACH ROW 
-- DECLARE
BEGIN
-- Modificar para que solo cambie lo que se indica, para que no vuelva en mayuscula 

-- Modificación en la actualización

IF UPDATING THEN

IF (:new.N_NOMCLIENTE <> :old.N_NOMCLIENTE) THEN
  :new.N_NOMCLIENTE := UPPER(:new.N_NOMCLIENTE);
  DBMS_OUTPUT.PUT_LINE('Ejecutando trigger1');
END IF;

-- aPLLIDO
IF (:new.N_APECLIENTE <> :old.N_APECLIENTE) THEN
  :new.N_APECLIENTE := UPPER(:new.N_APECLIENTE);
  DBMS_OUTPUT.PUT_LINE('Ejecutando trigger2');
END IF;

ELSE

:new.N_NOMCLIENTE := UPPER(:new.N_NOMCLIENTE);
:new.N_APECLIENTE := UPPER(:new.N_APECLIENTE);

END IF;


END TG_CAMBIAR_NOMBRE;


/


show errors;

exit;

