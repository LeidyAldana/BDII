
CREATE OR REPLACE PROCEDURE PR_BUSCARCLIENTE (
	pk_reserva   reserva.k_reserva%type,
        pn_nomcompleto  OUT   VARCHAR 
) AS 
   -- Declaración de variables
   lr_cliente  CLIENTE%rowtype;
BEGIN
	select * INTO lr_cliente  
	from CLIENTE c, RESERVA r
	where 
	c.K_NIT = r.K_NIT and 
	r.K_RESERVA=pk_reserva;
  
        pn_nomcompleto :=  lr_cliente.N_NOMCLIENTE||lr_cliente.N_APECLIENTE;

END  PR_BUSCARCLIENTE;



/

show errors;

exit;
