/*
  Bloque anónimo para ejecutar procedimiento
*/

SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED;
SET VERIFY OFF;

DECLARE
    ln_nomcompleto    VARCHAR(50);   
    lk_reserva        reserva.k_reserva%type := &r;

BEGIN
    
    PR_BUSCARCLIENTE(lk_reserva,ln_nomcompleto);
    DBMS_OUTPUT.put_line(ln_nomcompleto);

END;

/

exit;

