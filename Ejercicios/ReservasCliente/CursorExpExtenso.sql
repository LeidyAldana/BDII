

CREATE OR REPLACE PROCEDURE PR_BUSCARCLIENTE (
        pk_reserva   reserva.k_reserva%type,
        pn_nomcompleto  OUT   VARCHAR,
        pc_error        OUT   NUMBER,
        pm_error        OUT   VARCHAR
) AS
-- Cursor explicito de froma extensa
l_invalid   EXCEPTION;

CURSOR c_nombre IS
SELECT r.k_reserva as numero,
       c.n_nomCliente||' '||c.n_apecliente as nombre,
       r.f_reserva as fecha, v.k_matricula as placa, rv.I_ENTREGADO as entregado,
       r.V_TOTAL as total, r.V_TOTAL/(select count(*) FROM   Cliente c, Reserva r, Vehiculo v, Reserva_Vehiculo rv
WHERE  r.k_nit=c.k_nit
AND    v.k_matricula=rv.k_matricula
AND    r.k_reserva=rv.k_reserva   ) as promedio
FROM   Cliente c, Reserva r, Vehiculo v, Reserva_Vehiculo rv
WHERE  r.k_nit=c.k_nit
AND    v.k_matricula=rv.k_matricula
AND    r.k_reserva=rv.k_reserva
AND    r.k_reserva=pk_reserva
ORDER BY v.k_matricula;

lc_nombre    c_nombre%rowtype;

BEGIN

  OPEN  c_nombre;
 loop
  FETCH c_nombre INTO   lc_nombre;
 exit when c_nombre%notfound;
 dbms_output.put_line('  ');
 dbms_output.put_line('N. reserva = '||lc_nombre.numero);
 dbms_output.put_line('Nombre cliente = '||lc_nombre.nombre);
 dbms_output.put_line('Fecha reserva = '||lc_nombre.fecha);
 dbms_output.put_line('Placa = '||lc_nombre.placa);
 dbms_output.put_line('Entregado = '||lc_nombre.entregado);
 dbms_output.put_line('Valor total = '||lc_nombre.total);
 dbms_output.put_line('Valor promedio = '||lc_nombre.promedio);
 dbms_output.put_line('  ');

IF SQL%NOTFOUND THEN
  RAISE l_invalid;
END IF;

end loop;
  CLOSE c_nombre;

EXCEPTION
   WHEN TOO_MANY_ROWS THEN
     pm_error  := 'Demasiadas filas';
   WHEN ZERO_DIVIDE THEN
     pm_error  := 'División por cero';
   WHEN l_invalid THEN
     pm_error := SQLERRM;
     pc_error := SQLCODE;

END  PR_BUSCARCLIENTE;


/

show errors;

exit;
