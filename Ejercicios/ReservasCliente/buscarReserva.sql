
CREATE OR REPLACE PROCEDURE PR_BUSCARCLIENTE (
	pk_reserva   reserva.k_reserva%type,
        pn_nomcompleto  OUT   VARCHAR,
        pc_error        OUT   NUMBER, -- 0 Ok y 1 error
        pm_error        OUT   VARCHAR 
) AS 
BEGIN
        -- Variables de error
        pc_error    := 0;
        pm_error    := null;
        -- Cursor implicito
	select  N_NOMCLIENTE||' '||N_APECLIENTE INTO pn_nomcompleto
	from CLIENTE c, RESERVA r
	where 
	c.K_NIT = r.K_NIT and 
	r.K_RESERVA=pk_reserva;

EXCEPTION 
    WHEN NO_DATA_FOUND THEN
    -- dmbs, o las salidas 0 y 1  agregar pc error y pm erro
      dbms_output.put_line(' ');
      pc_error   :=  1;
      pm_error   :=  'Información no encontrada';
END  PR_BUSCARCLIENTE;



/

show errors;

exit;


