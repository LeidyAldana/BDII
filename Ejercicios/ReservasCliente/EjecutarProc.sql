/*
  Bloque anónimo para ejecutar procedimiento
*/

SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED;
SET VERIFY OFF;

DECLARE
    ln_nomcompleto    VARCHAR(50);   
    lk_reserva        reserva.k_reserva%type := &r;
    lc_error          NUMBER;
    lm_error          VARCHAR(50);
BEGIN
    
    --PR_BUSCARCLIENTE(lk_reserva,ln_nomcompleto,lc_error,lm_error);
    PR_BUSCARCLIENTE(lk_reserva,ln_nomcompleto,lc_error,lm_error);
    --DBMS_OUTPUT.put_line(ln_nomcompleto);
    -- pc error y pm error 27 Feb 2020
    DBMS_OUTPUT.put_line(lc_error);
    DBMS_OUTPUT.put_line(lm_error);
    --DBMS_OUTPUT.PUT_LINE('Termino bien');

EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Código del error: '||SQLCODE||' Mensaje: '||SQLERRM);
END;

/

show errors;

exit;

