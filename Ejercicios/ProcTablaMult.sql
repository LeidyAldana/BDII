create or replace PACKAGE PKG_Multiplicar  as

PROCEDURE pro_multiplicar(
   /* ==================================== */
   /* Objetivo: 
   /* ==================================== */
   n_ini     IN    NUMBER,
   n_fin     IN    NUMBER,
   l_inf     IN    NUMBER,
   l_sup     IN    NUMBER
);

END PKG_Multiplicar;

/
 

create or replace PACKAGE BODY PKG_Multiplicar as

PROCEDURE pro_multiplicar(
   /* ==================================== */
   /* Objetivo:
   /* ==================================== */
   n_ini     IN    NUMBER,
   n_fin     IN    NUMBER,
   l_inf     IN    NUMBER,
   l_sup     IN    NUMBER
)
AS
aux number;
BEGIN
    
    dbms_output.put_line(' Los limites son: '||n_ini||' '||n_fin||' '||l_inf||' '||l_sup);
    aux := l_inf;
    WHILE aux<=l_sup LOOP
        aux:=aux+1;
        FOR numero IN n_ini..n_fin  LOOP

            dbms_output.put(numero||' X '||aux||' = '||numero*aux||chr(9));

        END LOOP;
        dbms_output.put_line(' ');
    END LOOP;
END pro_multiplicar;


END PKG_Multiplicar;

/


exit;

