create or replace PACKAGE PK_XXX  as
-- Definicion de tipos y variables globales
CREATE OR REPLACE PROCEDURE pro_yyy (
   /* ==================================== */
   /* Objetivo: 
   /* ==================================== */
);

END PK_XXX;

/
 

create or replace PACKAGE BODY PK_XXX as

PROCEDURE pro_yyy(
   /* ==================================== */
   /* Objetivo:
   /* ==================================== */
)
AS
-- Variables 
BEGIN

END pro_yyy ;


END PK_XXX;

/


show errors;

exit;

