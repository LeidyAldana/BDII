create or replace PACKAGE PKG_Multiplicar  as

PROCEDURE pro_multiplicar(
   /* ==================================== */
   /* Objetivo: 
   /* ==================================== */
   n_ini     IN    NUMBER,
   n_fin     IN    NUMBER,
   l_inf     IN    NUMBER,
   l_sup     IN    NUMBER,
   pc_error OUT   NUMBER,
   pm_error  OUT   VARCHAR2
);

END PKG_Multiplicar;

/
 

create or replace PACKAGE BODY PKG_Multiplicar as

PROCEDURE pro_multiplicar(
   /* ==================================== */
   /* Objetivo:
   /* ==================================== */
   n_ini     IN    NUMBER,
   n_fin     IN    NUMBER,
   l_inf     IN    NUMBER,
   l_sup     IN    NUMBER,
   pc_error OUT   NUMBER,  -- 0 OK, 1 Fallo
   pm_error  OUT   VARCHAR2
)
AS
aux number;
BEGIN
    pc_error := 0;
    pm_error := null;
    dbms_output.put_line(' Los limites son: '||n_ini||' '||n_fin||' '||l_inf||' '||l_sup);
    aux := l_inf;
    WHILE aux<=l_sup LOOP
        aux:=aux+1;
        FOR numero IN n_ini..n_fin  LOOP

            dbms_output.put(numero||' X '||aux||' = '||numero*aux||chr(9));

        END LOOP;
        dbms_output.put_line(' ');
    END LOOP;
    pm_error := 'Termino exitosamente';

END pro_multiplicar;


END PKG_Multiplicar;

/


show errors;

exit;

