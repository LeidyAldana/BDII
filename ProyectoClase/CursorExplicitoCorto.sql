

CREATE OR REPLACE PROCEDURE PR_BUSCAR (
        pk_reserva   reserva.k_reserva%type,
        pn_nomcompleto  OUT   VARCHAR
) AS
BEGIN

-- Cursor explicito de froma extensa

CURSOR c_reservascliente IS
  SELECT r.k_reserva, c.k_nit, c.n_nomCliente||' '||c.n_apecliente, TO_CHAR(r.v_total,'$999.999.99')
  FROM reserva r, cliente c
  WHERE c.k_nit = r.k_nit
  ORDER BY r.K_RESERVA;


BEGIN
  FOR rc_reservascliente IN c_reservascliente LOOP
   -- Se procesa el cursor
   DBMS_OUTPUT_PUTLINE('Nombre '||rc_reservascliente.n_nomCliente);
  END LOOP;

  

END  PR_BUSCAR;



/

show errors;


