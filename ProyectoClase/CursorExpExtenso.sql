

CREATE OR REPLACE PROCEDURE PR_BUSCARCLIENTE (
        pk_reserva   reserva.k_reserva%type,
        pn_nomcompleto  OUT   VARCHAR
) AS
BEGIN

-- Cursor explicito de froma extensa

CURSOR c_nombre IS
  SELECT r.k_reserva, c.k_nit, c.n_nomCliente||' '||c.n_apecliente, TO_CHAR(r.v_total,'$999.999.99')
  FROM reserva r, cliente c
  WHERE c.k_nit = r.k_nit
  ORDER BY r.K_RESERVA;


lc_nombre    c_nombre%rowtype;
BEGIN
  OPEN  c_nombre
 loop
  FETCH c_nombre INTO   lc_nombre;
 exit when c_nombre%notfound;
  DBMS_OUTPUT_PUTLINE(lc_nombre.k_nit);
end loop;
  CLOSE c_nombre


END  PR_BUSCARCLIENTE;



/

show errors;


