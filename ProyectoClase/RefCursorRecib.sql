
CREATE OR REPLACE PROCEDURE PR_LISTAR_CLIENTES (
        pref_clientes   OUT   SYS_REFCURSOR
) AS

-- Cursores debiles


BEGIN

  OPEN pref_clientes FOR SELECT r.k_reserva, c.k_nit, c.n_nomCliente||' '||c.n_apecliente--, TO_CHAR(r.v_total,'$999.999.99')
  FROM reserva r, cliente c
  WHERE c.k_nit = r.k_nit
  ORDER BY r.K_RESERVA;


END PR_LISTAR_CLIENTES;



/

show errors;


