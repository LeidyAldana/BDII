-- Variables asociadas a los cursores %isopen %notfound %found %rowcount
-- Hacer cursor para obtener lista:
-- N. reserva | Nit | NombreCompleto | valorTotal | iva
-- ordenar x numero de reserva y sacar total del valor total y el iva

SELECT r.k_reserva, c.k_nit, c.n_nomCliente||' '||c.n_apecliente, r.v_total
FROM reserva r, cliente c
WHERE c.k_nit = r.k_nit
ORDER BY r.K_RESERVA;
