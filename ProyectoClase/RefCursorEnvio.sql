DECLARE

 lref_clientes   SYS_REFCURSOR;
 l_nomcliente    CLIENTE.N_NOMCLIENTE%TYPE;
 l_apecliente    CLIENTE.N_APECLIENTE%TYPE;

BEGIN

 PR_LISTAR_CLIENTES(lref_clientes);
 FETCH lref_clientes INTO l_nomcliente, l_apecliente;

 EXIT WHEN lref_clientes%NOTFOUND;
 DBMS_OUTPUT_PUTLINE('Nombre '||lref_clientes.l_nomcliente); 

END;

/

show errors;


exit;

