-- Cursor explicito

CURSOR c_nombre IS
  SELECT r.k_reserva, c.k_nit, c.n_nomCliente||' '||c.n_apecliente, r.v_total
  FROM reserva r, cliente c
  WHERE c.k_nit = r.k_nit
  ORDER BY r.K_RESERVA;


lc_nombre    c_nombre%rowtype;
BEGIN
  OPEN  c_nombre
 loop
  FETCH c_nombre INTO   lc_nombre;
 exit when c_nombre%notfound;
  DBMS_OUTPUT_PUTLINE(lc_nombre.k_nit);
end loop;
  CLOSE c_nombre

