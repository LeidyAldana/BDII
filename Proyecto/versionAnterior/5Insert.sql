Insert into AdminNatame.GradoRV (K_GRADORV,T_COMISIONVENTAS) values ('BEGINEER',0.2);
Insert into AdminNatame.GradoRV (K_GRADORV,T_COMISIONVENTAS) values ('JUNIOR',0.3);
Insert into AdminNatame.GradoRV (K_GRADORV,T_COMISIONVENTAS) values ('SENIOR',0.4);
Insert into AdminNatame.GradoRV (K_GRADORV,T_COMISIONVENTAS) values ('MASTER',0.5);

Insert into ADMINNATAME.PAIS (K_PAIS,C_NOMBRE) values (AdminNatame.Seq_Pais.nextval,'United States of America');
Insert into ADMINNATAME.PAIS (K_PAIS,C_NOMBRE) values (AdminNatame.Seq_Pais.nextval,'Colombia');
Insert into ADMINNATAME.PAIS (K_PAIS,C_NOMBRE) values (AdminNatame.Seq_Pais.nextval,'Chile');
Insert into ADMINNATAME.PAIS (K_PAIS,C_NOMBRE) values (AdminNatame.Seq_Pais.nextval,'Canada');

Insert into ADMINNATAME.REGION (K_REGION,C_NOMBREREGION,K_PAIS) values ('1','New York','1');
Insert into ADMINNATAME.REGION (K_REGION,C_NOMBREREGION,K_PAIS) values ('2','Bogot ','2');
Insert into ADMINNATAME.REGION (K_REGION,C_NOMBREREGION,K_PAIS) values ('3','Santiago','3');
Insert into ADMINNATAME.REGION (K_REGION,C_NOMBREREGION,K_PAIS) values ('4','Fort Lauderdale','1');

Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE) values (AdminNatame.Seq_SubCategoria.nextval,'NUTRICION');
Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE) values (AdminNatame.Seq_SubCategoria.nextval,'CUIDADO PERSONAL');

Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE,D_RESUMEN,K_CATEGORIA) values (AdminNatame.Seq_SubCategoria.nextval,'SOLIDO','Alimentos solidos de nutricion','1');
Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE,D_RESUMEN,K_CATEGORIA) values (AdminNatame.Seq_SubCategoria.nextval,'LIMPADOR','Limpiadores para piel','2');
Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE,D_RESUMEN,K_CATEGORIA) values (AdminNatame.Seq_SubCategoria.nextval,'BEBIDA','Bebidas de nutricion','1');
Insert into ADMINNATAME.SUBCATEGORIA (K_SUBCATEGORIA,C_NOMBRE,D_RESUMEN,K_CATEGORIA) values (AdminNatame.Seq_SubCategoria.nextval,'TONIFICADOR','Tonificadores para piel','2');

Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Batido Nutricional Formula 1','3',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Bebida Herbal','3',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Aloe Concentrado','3',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'H24 rebuild','3',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Barra de proteina','4',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Barra nutricional express','4',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Complejo Vitaminico','4',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'HerbaNatame PLUS','4',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Limpiador Relajante de S bila','5',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Limpiador Citrico','5',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Mascarilla purificadora','5',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Sorum Herb','5',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Tonificador energizante','6',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Crema hidratante','6',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Mascarilla purificadora','6',TO_DATE('2019/10/4','yyyy-mm-dd'));
Insert into ADMINNATAME.PRODUCTO values (AdminNatame.SEQ_Producto.nextval,'Crema Protectora Solar','6',TO_DATE('2019/10/4','yyyy-mm-dd'));


INSERT INTO REPRESENTANTEVENTAS VALUES(1,'CC','ADMINNATAME','ADMIN@','A',TO_DATE('1999-10-5','YYYY-MM-DD'),TO_DATE('2019-10-5','YYYY-MM-DD'),1,'CR 7','N','MASTER',1,NULL,NULL,'ADMINNATAME');


insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,1,1,5000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,2,1,5000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,3,1,4000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,4,1,2000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,1,2,3000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,2,2,4000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,3,2,1000,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,4,2,2500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,1,3,2500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,2,3,2500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,3,3,5500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,4,3,5500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,1,4,5500,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,2,4,5200,0.19);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,3,4,5200,0.18);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,4,4,4200,0.18);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,1,5,4200,0.18);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,2,6,4200,0.18);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,3,7,4200,0.18);

insert into inventario values(adminnatame.SEQ_Inventario.nextval,5000,4,8,4200,0.18);


INSERT INTO CALIFICACION VALUES(0,'MUY MALO');

INSERT INTO CALIFICACION VALUES(1,'MALO');

INSERT INTO CALIFICACION VALUES(2,'MEDIANAMENTE MALO');

INSERT INTO CALIFICACION VALUES(3,'MEDIANAMENTE BUENO');

INSERT INTO CALIFICACION VALUES(4,'BUENO');

INSERT INTO CALIFICACION VALUES(5,'MUY BUENO');


COMMIT;

/*CREAR VISTA*/

CREATE OR REPLACE VIEW ADMINNATAME.INVENTARIOVIEW 
AS (SELECT I.K_INVENTARIO, P.K_PRODUCTO,P.V_NOMBRE,I.N_CANTIDAD,I.T_PRECIO,I.K_REGION FROM PRODUCTO P, INVENTARIO I WHERE I.K_PRODUCTO=P.K_PRODUCTO)
;

DROP PUBLIC SYNONYM INVENTARIOVIEW;
CREATE PUBLIC SYNONYM INVENTARIOVIEW FOR ADMINNATAME.INVENTARIOVIEW;