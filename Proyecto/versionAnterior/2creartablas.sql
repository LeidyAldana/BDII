/* Create Tables */

DROP TABLE   AdminNatame.Asociacion;
DROP TABLE   AdminNatame.Calificacion;
DROP TABLE   AdminNatame.Cliente;
DROP TABLE   AdminNatame.DetallePedido;
DROP TABLE   AdminNatame.GradoRV;
DROP TABLE   AdminNatame.Inventario;
DROP TABLE   AdminNatame.Pais;
DROP TABLE   AdminNatame.Pedido;
DROP TABLE   AdminNatame.Producto;
DROP TABLE   AdminNatame.RepresentanteVentas;
DROP TABLE   AdminNatame.Subcategoria;
DROP TABLE   AdminNatame.Region;


CREATE TABLE  AdminNatame.Asociacion
(
	F_INICIO DATE NOT NULL,    -- Fecha en la cual se asigno ese representante de ventas para ese cliente
	F_FIN DATE NULL,    -- Fecha en la cual el representante de ventas termino con la asesoria prestada a ese cliente
	K_SECUENCIA NUMBER(10) NOT NULL,    -- Llave primaria de esta tabla
	K_IDCLIENTE NUMBER(12) NOT NULL,    -- Cedula del representante de ventas
	K_IDRV NUMBER(12) NOT NULL,
	K_TIPOIDCLIENTE VARCHAR2(2) NOT NULL,
	K_TIPOIDRV VARCHAR2(2) NOT NULL
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  ADMINNATAME.Calificacion
(
	K_CALIFICACION NUMBER(1) NOT NULL,    -- Calificacion que podra asignarse a los representantes de ventas
	D_VALORACION VARCHAR2(50) NOT NULL    -- Descripcion de la calificacion asignada
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.Cliente
(
	K_IDENTIFICACION NUMBER(12) NOT NULL,    -- Cedula del cliente que identificara de manera unica a cada uno
	C_NOMBRE VARCHAR2(50) NOT NULL,    -- Primer y segundo nombre del cliente
	C_APELLIDO VARCHAR2(50) NOT NULL,    -- Primer y segundo apellido del cliente
	N_TELEFONO NUMBER(12) NOT NULL,    -- Telefono de contacto del cliente
	C_DIRECCION VARCHAR2(50) NOT NULL,    -- Direccion fisica del cliente
	C_CIUDAD VARCHAR2(50) NOT NULL,    -- Ciudad en la cual vive el cliente
	C_CORREO VARCHAR2(50) NULL,    -- Correo electronico del cliente
	K_TIPOID VARCHAR2(5) NOT NULL,
	C_USUARIO VARCHAR2(20) NOT NULL
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.DetallePedido
(
	N_CANTIDAD NUMBER(8) NOT NULL,    -- Cantidad del producto pedido
	K_PEDIDO NUMBER(8) NOT NULL,    -- Llave foranea que referencia al pedido
	K_INVENTARIO NUMBER(8) NOT NULL    -- Llave foranea que referencia al producto en cierta region
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.GradoRV
(
	K_GRADORV VARCHAR2(10) NOT NULL,    -- Llave primaria que contiene el nombre del grado
	T_COMISIONVENTAS NUMBER(2,2) NOT NULL    -- Porcentaje de la comision de ventas que esta determinado por el grado
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.Inventario
(
	K_INVENTARIO NUMBER(8) NOT NULL,    -- Llave primaria de esta tabla
	T_PRECIO NUMBER(8,2) NOT NULL,    -- Precio del producto en la region
	K_REGION NUMBER(8) NOT NULL,    -- Llave foranea que referencia determinada region
	K_PRODUCTO NUMBER(8) NOT NULL,    -- Llave foranea que referencia determinado producto
	N_CANTIDAD NUMBER(8) NOT NULL,    -- Cantidad del producto
	T_IMPUESTO NUMBER(2,2) NOT NULL    -- Iva del producto para dicha region
)
TABLESPACE	Def_natame
;

CREATE TABLE  AdminNatame.Pais
(
	K_PAIS NUMBER(8) NOT NULL,    -- Llave primaria para la tabla Pais
	C_NOMBRE VARCHAR2(50) NOT NULL    -- Campo que contiene el nombre del pais
)
TABLESPACE	Def_natame
;

CREATE TABLE  AdminNatame.Pedido
(
	K_PEDIDO NUMBER(8) NOT NULL,    -- Llave primaria de esta tabla
	I_ESTADOPEDIDO VARCHAR2(10) NOT NULL,    -- Almacena el estado actual del pedido
	K_CALIFICACION NUMBER(1) NULL,    -- Guarda la calificacion del pedido, la cual es dada por el cliente
	F_PEDIDO DATE NOT NULL,    -- Almacena la fecha en la cual se realiza el pedido
	I_MODOPAGO VARCHAR2(3) NOT NULL,    -- Modo de pago con el cual se cancela el pedido
	T_VALOR NUMBER(10,2) NULL,    -- Monto pagado por el cliente para ese pedido
	K_ASOCIACION NUMBER(10) NOT NULL
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.Producto
(
	K_PRODUCTO NUMBER(8) NOT NULL,    -- Columna que almacena la llave primaria del conjunto de entidades producto de los diferentes productos a vender en la empresa NatAme
	V_NOMBRE VARCHAR2(30) NOT NULL,    -- Nombre del producto a vender
	K_SUBCATEGORIA NUMBER(8) NOT NULL,
	F_CLASIFICACION DATE NULL
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.Region
(
	K_REGION NUMBER(8) NOT NULL,    -- Llave primaria de la tabla Region que ser� llenado por una secuencia
	C_NOMBREREGION VARCHAR2(50) NOT NULL,    -- Campo que describe el nombre de la regi�n
	K_PAIS NUMBER(8) NULL    -- Llave foranea que hace referencia a la llave primaria de la tabla pais
)
TABLESPACE	DEF_NATAME
;

CREATE TABLE  AdminNatame.RepresentanteVentas
(
	K_IDENTIFICACION NUMBER(12) NOT NULL,    -- Columna que contiene el numero de identificacion de cada uno de los representantes de ventas
    K_TIPOID VARCHAR2(2) NOT NULL,
	C_NOMBRE VARCHAR2(50) NOT NULL,    -- Nombre del representante de ventas
	C_CORREOELECTRONICO VARCHAR2(50) NULL,    -- Correo electronico del representante de ventas
	I_GENERO VARCHAR2(1) NOT NULL,    -- Genero del representante de ventas, puede ser masculino o femenino
	F_NACIMIENTO DATE NOT NULL,    -- Fecha de nacimiento del representante de ventas
	F_CONTRATO DATE NOT NULL,    -- Fecha en la cual se firmo el contrato entre la empresa y el representante de ventas
	N_TELEFONO NUMBER(10) NOT NULL,    -- Telefono en el cual se puede contactar al representante de ventas
	C_DIRECCION VARCHAR2(30) NOT NULL,    -- Direccion fisica donde puede encontrarse al representante de ventas
	I_ESDIRECTOR VARCHAR2(1) NOT NULL,    -- Campo que indica si el representante de ventas es o no es director
	K_GRADORV VARCHAR2(10) NULL,    -- Llave que hace referencia al grado del representante de ventas
	K_REGION NUMBER(8) NULL,    -- Llave primaria que referencia al representante de ventas que registro al repreeentante de ventas de cada registro
    K_RVMTIPOID VARCHAR2(2) NULL,
    K_RVMID NUMBER(12) NULL,
    C_USUARIO VARCHAR2(20) NOT NULL
)
TABLESPACE	Def_natame
;

CREATE TABLE  AdminNatame.Subcategoria
(
	K_SUBCATEGORIA NUMBER(8) NOT NULL,    -- Llave primaria que identificara a cada una de las susbcategorias
	C_NOMBRE VARCHAR2(25) NOT NULL,    -- Campo que almacena el nombre de cada una de las subcategorias de los productos vendidos en NatAme
	D_RESUMEN VARCHAR2(50) NULL,    -- Descripcion de la subcategoria 
	K_CATEGORIA NUMBER(8) NULL    -- Llave foranea que referencia 
)
TABLESPACE	DEF_NATAME
;

/* Create Comments, Sequences and Triggers for Autonumber Columns */

COMMENT ON TABLE  AdminNatame.Asociacion IS 'Registra la informaci&#243;n correspondiente a la relacion entre el cliente y el representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.Asociacion.F_INICIO IS 'Fecha en la cual se asigno ese representante de ventas para ese cliente'
;

COMMENT ON COLUMN  AdminNatame.Asociacion.F_FIN IS 'Fecha en la cual el representante de ventas termino con la asesoria prestada a ese cliente'
;

COMMENT ON COLUMN  AdminNatame.Asociacion.K_SECUENCIA IS 'Llave primaria de esta tabla'
;

COMMENT ON COLUMN  AdminNatame.Asociacion.K_IDCLIENTE IS 'Cedula del representante de ventas'
;

COMMENT ON TABLE  ADMINNATAME.Calificacion IS 'Contiene la informacion relacionada a la calificacion que puede dar un cliente a un representante de ventas'
;

COMMENT ON COLUMN  ADMINNATAME.Calificacion.K_CALIFICACION IS 'Calificacion que podra asignarse a los representantes de ventas'
;

COMMENT ON COLUMN  ADMINNATAME.Calificacion.D_VALORACION IS 'Descripcion de la calificacion asignada'
;

COMMENT ON TABLE  AdminNatame.Cliente IS 'Almacena los diferentes clientes que hacen uso del sistema'
;

COMMENT ON COLUMN  AdminNatame.Cliente.K_IDENTIFICACION IS 'Cedula del cliente que identificara de manera unica a cada uno'
;

COMMENT ON COLUMN  AdminNatame.Cliente.C_NOMBRE IS 'Primer y segundo nombre del cliente'
;

COMMENT ON COLUMN  AdminNatame.Cliente.C_APELLIDO IS 'Primer y segundo apellido del cliente'
;

COMMENT ON COLUMN  AdminNatame.Cliente.N_TELEFONO IS 'Telefono de contacto del cliente'
;

COMMENT ON COLUMN  AdminNatame.Cliente.C_DIRECCION IS 'Direccion fisica del cliente'
;

COMMENT ON COLUMN  AdminNatame.Cliente.C_CIUDAD IS 'Ciudad en la cual vive el cliente'
;

COMMENT ON COLUMN  AdminNatame.Cliente.C_CORREO IS 'Correo electronico del cliente'
;

COMMENT ON TABLE  AdminNatame.DetallePedido IS 'Referencia los productos por region involucrados en cada uno de los pedidos'
;

COMMENT ON COLUMN  AdminNatame.DetallePedido.N_CANTIDAD IS 'Cantidad del producto pedido'
;

COMMENT ON COLUMN  AdminNatame.DetallePedido.K_PEDIDO IS 'Llave foranea que referencia al pedido'
;

COMMENT ON COLUMN  AdminNatame.DetallePedido.K_INVENTARIO IS 'Llave foranea que referencia al producto en cierta region'
;

COMMENT ON TABLE  AdminNatame.GradoRV IS 'Contiene la informacion correspondiente a los diferentes grados que puede tener un representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.GradoRV.K_GRADORV IS 'Llave primaria que contiene el nombre del grado'
;

COMMENT ON COLUMN  AdminNatame.GradoRV.T_COMISIONVENTAS IS 'Porcentaje de la comision de ventas que esta determinado por el grado'
;

COMMENT ON TABLE  AdminNatame.Inventario IS 'Referencia cada producto con su respectiva region'
;

COMMENT ON COLUMN  AdminNatame.Inventario.K_INVENTARIO IS 'Llave primaria de esta tabla'
;

COMMENT ON COLUMN  AdminNatame.Inventario.T_PRECIO IS 'Precio del producto en la region'
;

COMMENT ON COLUMN  AdminNatame.Inventario.K_REGION IS 'Llave foranea que referencia determinada region'
;

COMMENT ON COLUMN  AdminNatame.Inventario.K_PRODUCTO IS 'Llave foranea que referencia determinado producto'
;

COMMENT ON COLUMN  AdminNatame.Inventario.N_CANTIDAD IS 'Cantidad del producto'
;

COMMENT ON COLUMN  AdminNatame.Inventario.T_IMPUESTO IS 'Iva del producto para dicha region'
;

COMMENT ON TABLE  AdminNatame.Pais IS 'Tabla que contiene los diferentes paises con los cuales trabaja la empresa NatAme'
;

COMMENT ON COLUMN  AdminNatame.Pais.K_PAIS IS 'Llave primaria para la tabla Pais'
;

COMMENT ON COLUMN  AdminNatame.Pais.C_NOMBRE IS 'Campo que contiene el nombre del pais'
;

COMMENT ON TABLE  AdminNatame.Pedido IS 'Guarda los registros de cada pedido realizado por un cliente'
;

COMMENT ON COLUMN  AdminNatame.Pedido.K_PEDIDO IS 'Llave primaria de esta tabla'
;

COMMENT ON COLUMN  AdminNatame.Pedido.I_ESTADOPEDIDO IS 'Almacena el estado actual del pedido'
;

COMMENT ON COLUMN  AdminNatame.Pedido.K_CALIFICACION IS 'Guarda la calificacion del pedido, la cual es dada por el cliente'
;

COMMENT ON COLUMN  AdminNatame.Pedido.F_PEDIDO IS 'Almacena la fecha en la cual se realiza el pedido'
;

COMMENT ON COLUMN  AdminNatame.Pedido.I_MODOPAGO IS 'Modo de pago con el cual se cancela el pedido'
;

COMMENT ON COLUMN  AdminNatame.Pedido.T_VALOR IS 'Monto pagado por el cliente para ese pedido'
;

COMMENT ON TABLE  AdminNatame.Producto IS 'Almacena los diferentes productos que ser�n vendidos por la empresa NatAme'
;

COMMENT ON COLUMN  AdminNatame.Producto.K_PRODUCTO IS 'Columna que almacena la llave primaria del conjunto de entidades producto de los diferentes productos a vender en la empresa NatAme'
;

COMMENT ON COLUMN  AdminNatame.Producto.V_NOMBRE IS 'Nombre del producto a vender'
;

COMMENT ON TABLE  AdminNatame.Region IS 'Contiene las diferentes regiones de cada pais en los cuales se comercializan los productos de la empresa'
;

COMMENT ON COLUMN  AdminNatame.Region.K_REGION IS 'Llave primaria de la tabla Region que ser� llenado por una secuencia'
;

COMMENT ON COLUMN  AdminNatame.Region.C_NOMBREREGION IS 'Campo que describe el nombre de la regi�n'
;

COMMENT ON COLUMN  AdminNatame.Region.K_PAIS IS 'Llave foranea que hace referencia a la llave primaria de la tabla pais'
;

COMMENT ON TABLE  AdminNatame.RepresentanteVentas IS 'Contiene el registro de cada uno de los representantes de ventas que interactuan con el sistema'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.K_IDENTIFICACION IS 'Columna que contiene el numero de identificacion de cada uno de los representantes de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.C_NOMBRE IS 'Nombre del representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.C_CORREOELECTRONICO IS 'Correo electronico del representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.I_GENERO IS 'Genero del representante de ventas, puede ser masculino o femenino'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.F_NACIMIENTO IS 'Fecha de nacimiento del representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.F_CONTRATO IS 'Fecha en la cual se firmo el contrato entre la empresa y el representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.N_TELEFONO IS 'Telefono en el cual se puede contactar al representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.C_DIRECCION IS 'Direccion fisica donde puede encontrarse al representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.I_ESDIRECTOR IS 'Campo que indica si el representante de ventas es o no es director'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.K_GRADORV IS 'Llave que hace referencia al grado del representante de ventas'
;

COMMENT ON COLUMN  AdminNatame.RepresentanteVentas.K_REGION IS 'Llave primaria que referencia al representante de ventas que registro al repreeentante de ventas de cada registro'
;

COMMENT ON TABLE  AdminNatame.Subcategoria IS 'Contiene la informacion de las subcategorias que se tienen en la aplicacion para clasificacion de productos, y de manera recursiva almacena las categorias'
;

COMMENT ON COLUMN  AdminNatame.Subcategoria.K_SUBCATEGORIA IS 'Llave primaria que identificara a cada una de las susbcategorias'
;

COMMENT ON COLUMN  AdminNatame.Subcategoria.C_NOMBRE IS 'Campo que almacena el nombre de cada una de las subcategorias de los productos vendidos en NatAme'
;

COMMENT ON COLUMN  AdminNatame.Subcategoria.D_RESUMEN IS 'Descripcion de la subcategoria '
;

COMMENT ON COLUMN  AdminNatame.Subcategoria.K_CATEGORIA IS 'Llave foranea que referencia '
;
