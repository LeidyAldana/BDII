/*----------------------------------------
  Universidad Distrital FJC. BD II
  Autores: Leidy Aldana
           Esteban Romero
           Camilo Guaba
           José Gonzalez
---------------------------------------------*/

create or replace TRIGGER TG_CAMBIOSALDOCUENTA 
AFTER INSERT OR UPDATE OF V_SALDO OR DELETE ON CUENTA 
FOR EACH ROW
DECLARE
  /* Objetivo:
    Auditar el cambio de el valor de saldo en las cuentas
  */
  
   lk_audito      AUDITORIAPagos.k_audito%TYPE;
   lc_campoact    AUDITORIAPagos.c_campoact%TYPE;
   ln_vanterior   AUDITORIAPagos.n_valorant%type;
   ln_vnuevo      AUDITORIAPagos.n_valornuevo%type;
BEGIN
   -- Modificacion en la actualización
  select  SEQ_AUDITORIAPagos.NEXTVAL into lk_audito from dual;

  -- Se evalua cual es el valor que está cambiando
  IF (:new.V_SALDO <> :old.V_SALDO) THEN
     lc_campoact  :=   'V_SALDO';
     ln_vanterior :=   :old.V_SALDO;
     ln_vnuevo    :=   :new.V_SALDO;
  ELSE
     lc_campoact  :=   'V_SALDO';
     ln_vanterior :=   0;
     ln_vnuevo    :=  :new.V_SALDO;
  END IF;


     INSERT INTO auditoriapagos VALUES (lk_audito,
                                   SYSDATE,
                                   USER,
                                   ln_vanterior,
                                   ln_vnuevo,
                                   lc_campoact
      );
END;

/

show errors;

exit;
        