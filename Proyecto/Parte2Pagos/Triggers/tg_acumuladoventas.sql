/*----------------------------------------
  Universidad Distrital FJC. BD II
  Autores: Leidy Aldana
           Esteban Romero
           Camilo Guaba
           José Gonzalez
---------------------------------------------*/

create or replace TRIGGER tg_acumuladoventas BEFORE
    INSERT OR UPDATE OF k_pedido ON pedido
    FOR EACH ROW
DECLARE
  
  /* Objetivo:
    Actualizar el campo n_acumuladoventas para la tabla RV
    y así mantener integridad
  */
    ln_valornuevacompra    NUMBER;
    ln_acumuladoanterior   NUMBER;
    
    TYPE gtr_rv IS RECORD (
        k_identificacion    representanteventas.k_identificacion%TYPE,
        k_tipoid            representanteventas.k_tipoid%TYPE,
        n_acumuladoventas   representanteventas.n_acumuladoventas%TYPE
    );
    
    ln_rvmodificado        gtr_rv;
    
BEGIN

    ln_valornuevacompra := 0;
    ln_acumuladoanterior := 0;
    SELECT
        rv.k_identificacion,
        rv.k_tipoid,
        rv.n_acumuladoventas
    INTO ln_rvmodificado
    FROM
        representanteventas   rv,
        asociacion ac,
        pedido                pd
    WHERE
        rv.k_identificacion = ac.k_idrv
        AND rv.k_tipoid = ac.k_tipoidrv
        AND pd.k_pedido = k_pedido
        AND ROWNUM <2;

  -- Se halla el valor de la nueva venta

    ln_valornuevacompra := pk_factura.fu_totalizar_carrito(:new.k_pedido);

  -- Se halla el valor anterior del acumulado en ventas anterior
    ln_acumuladoanterior := ln_rvmodificado.n_acumuladoventas;

  -- Se actualiza tal que el nuevo valor es el anterior + el nuevo 
    UPDATE representanteventas
    SET
        n_acumuladoventas = ( ln_valornuevacompra + ln_acumuladoanterior )
    WHERE
        k_identificacion = ln_rvmodificado.k_identificacion
        AND k_tipoid = ln_rvmodificado.k_tipoid
        AND ROWNUM < 2;

END;


/

show errors;

exit;
        