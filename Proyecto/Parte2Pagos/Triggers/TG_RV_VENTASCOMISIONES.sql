/*----------------------------------------
  Universidad Distrital FJC. BD II
  Autores: Leidy Aldana
           Esteban Romero
           Camilo Guaba
           José Gonzalez
---------------------------------------------*/

CREATE OR REPLACE TRIGGER "TG_RV_VENTASCOMISIONES" AFTER
    UPDATE OF k_gradorv, n_acumuladoventas OR INSERT ON representanteventas
    FOR EACH ROW
DECLARE
  /* Objetivo:
    Auditar los campos K_GRADORV, N_ACUMULADOVENTAS
  */
    lk_audito      auditoria.k_audito%TYPE;
    lc_campoact    auditoria.c_campoact%TYPE;
    ln_vanterior   auditoria.n_valorant%TYPE;
    ln_vnuevo      auditoria.n_valornuevo%TYPE;
BEGIN
    lk_audito := '0';
    lc_campoact := 'RV';
    ln_vanterior := '0.00';
    ln_vnuevo := '0.00';
   
  -- Modificacion en la actualización
    SELECT
        seq_auditoria.NEXTVAL
    INTO lk_audito
    FROM
        dual;

  -- Se evalua cual es el valor que está cambiando

    IF ( :new.k_gradorv <> :old.k_gradorv ) THEN
        lc_campoact :=   'K_GRADORV';
        ln_vanterior := :old.k_gradorv;
        ln_vnuevo    := :new.k_gradorv;
  ELSIF ( :new.n_acumuladoventas <> :old.n_acumuladoventas ) THEN
        lc_campoact := 'N_ACUMULADOVENTAS';
        ln_vanterior := :old.n_acumuladoventas;
        ln_vnuevo := :new.n_acumuladoventas;
    ELSE
        IF ( :new.n_acumuladoventas <> NULL ) THEN                
            ln_vnuevo    := :new.n_acumuladoventas;
        END IF;
        lc_campoact := 'N_ACUMULADOVENTAS';
    END IF;

    INSERT INTO auditoria VALUES (
        lk_audito,
        SYSDATE,
        user,
        ln_vanterior,
        ln_vnuevo,
        lc_campoact
    );

END;


/

show errors;

exit;
        