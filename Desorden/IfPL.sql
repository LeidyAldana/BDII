/*
 Dados 3 números (a,b,c) encontrar el número mayor
 */
SET SERVEROUTPUT ON;
SET VERIFY OFF;
DECLARE
uno NUMBER := &uno;
dos NUMBER:= &dos;
tres NUMBER:= &tres;
mayor NUMBER;
BEGIN    
    dbms_output.put_line(' Los números son: '||uno||' '||dos||' '||tres);
    IF uno>dos THEN        
        IF dos>tres THEN
            -- se asume que uno>tres por transitoriedad
            --dbms_output.put_line(' El mayor es: '||uno);            
            mayor:=uno;
        ELSIF tres>dos THEN
            IF uno>tres THEN
                --dbms_output.put_line(' El mayor es: '||uno);
                mayor:=uno;
            ELSE
                --dbms_output.put_line(' El mayor es: '||tres);
                mayor:=tres;
            END IF;    
        END IF;           
    ELSIF tres>uno THEN
        --dbms_output.put_line(' El mayor es: '||tres);
        mayor:=tres;
    ELSE
        --dbms_output.put_line(' El mayor es: '||dos);
        mayor:=dos;
    END IF;
    dbms_output.put_line(' El mayor es: '||mayor);
END;
/


