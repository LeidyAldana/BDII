

CREATE OR REPLACE FUNCTION FU_FACTIT(
    pnum   INTEGER)  RETURN INTEGER
AS
-- Declaración de variables locales

    l_fact     INTEGER;
    l_aux     INTEGER;

BEGIN

 l_fact    :=  1;
 l_aux     :=  pnum;
 
 IF (l_aux>0) THEN
   WHILE (l_aux > 0) 
   LOOP
      l_fact :=  l_fact*l_aux;
      l_aux  :=  l_aux - 1;
   END LOOP;
 ELSE
   DBMS_OUTPUT.PUT_LINE('El número ingresado debe ser mayor que cero');
 END IF;

 RETURN l_fact;

END FU_FACTIT;



/


show errors;

exit;
