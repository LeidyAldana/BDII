

connect sys/12345 as sysdba

CREATE VIEW NATAME_USR_ROLE AS 
(
    SELECT GRANTEE,GRANTED_ROLE,ADMIN_OPTION FROM DBA_ROLE_PRIVS WHERE GRANTED_ROLE IN ('R_RV','R_RVM','R_CLIENTE')
);


CREATE VIEW NATAME_ROLE_PRIVS AS
(
select GRANTEE, TABLE_NAME, PRIVILEGE from dba_tab_privs 
where table_name IN (SELECT DISTINCT OBJECT_NAME FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND OWNER = 'ADMINNATAME')
AND GRANTEE IN ('R_RV','R_RVM','R_CLIENTE')
);

CREATE PUBLIC SYNONYM NATAME_USR_ROLE FOR NATAME_USR_ROLE;
CREATE PUBLIC SYNONYM NATAME_ROLE_PRIVS FOR NATAME_ROLE_PRIVS;

GRANT SELECT ON NATAME_USR_ROLE TO ADMINNATAME;
GRANT SELECT ON NATAME_ROLE_PRIVS TO ADMINNATAME;
