/*
 Tabla de multiplicar
 */
--SET SERVEROUTPUT ON;
SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED
SET VERIFY OFF;
DECLARE
n_ini NUMBER;
n_fin NUMBER;
l_inf NUMBER;
l_sup NUMBER;
aux NUMBER;
BEGIN
    n_ini:=&n_ini;
    n_fin:=&n_fin;
    l_inf:=&l_inf;
    l_sup:=&l_sup;
    dbms_output.put_line(' Los límites son: '||n_ini||' '||n_fin||' '||l_inf||' '||l_sup);
    aux := l_inf;
    WHILE aux<=l_sup LOOP
        aux:=aux+1;
        FOR numero IN n_ini..n_fin  LOOP
                        
            dbms_output.put(numero||' X '||aux||' = '||numero*aux||chr(9));
                                    
        END LOOP;
        dbms_output.put_line(' ');
    END LOOP;
END;
/


