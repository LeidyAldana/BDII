/*
  Bloque anónimo para ejecutar procedimiento
*/

SET SERVEROUTPUT ON SIZE 1000000 FORMAT WRAPPED;
SET VERIFY OFF;

DECLARE

    ln_ini     NUMBER;
    ln_fin     NUMBER;
    ll_inf     NUMBER;
    ll_sup     NUMBER;
    lc_error   NUMBER;
    lm_error   VARCHAR2(50);

BEGIN

    ln_ini     :=1; 
    ln_fin     :=10; 
    ll_inf     :=1;
    ll_sup     :=10;
    lc_error   :=0;
    lm_error   :='Ok';

    --ln_ini     :=&ln_ini; 
    --ln_fin     :=&ln_fin; 
    --ll_inf     :=&ll_inf;
    --ll_sup     :=&ll_sup;
    --lc_error   :=&lc_error;
    --lm_error   :=&lm_error;

    PKG_Multiplicar.pro_multiplicar(1,10,1,10, 0, 'ok');
    DBMS_OUTPUT.put_line(lm_error);

END;

/


exit;


